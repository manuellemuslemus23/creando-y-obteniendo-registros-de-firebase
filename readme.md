## Adaptando codigo a mi propia base de datos en Firebase
 
 
 lo primero en entrar al sitio oficial de firebase https://firebase.google.com/  y vamos a la consola
 
 <p align="center"> 
 <img src="https://firebasestorage.googleapis.com/v0/b/prueba-6ec56.appspot.com/o/1.png?alt=media&token=e0062c75-b7e1-4b72-8c9b-65a6ec963998" width="1000"> 
 </p> 
 
  Ahora solo debemos iniciar sesion con una cuenta de Google
  
 <p align="center"> 
 <img src="https://firebasestorage.googleapis.com/v0/b/prueba-6ec56.appspot.com/o/iniciarsesion.png?alt=media&token=0160dc2b-bd32-4a84-bb53-b3f77d8a286a" width="1000"> 
 </p> 
    
  Ahora debemos crear un nuevo proyecto en firebase
  
 <p align="center"> 
 <img src="https://firebasestorage.googleapis.com/v0/b/prueba-6ec56.appspot.com/o/2.png?alt=media&token=48ef6fd9-d5e4-428b-879e-7c30809c09f7" width="1000"> 
 </p> 
 
 llenamos los campos correspondientes para crear el proyecto
   
 <p align="center"> 
 <img src="https://firebasestorage.googleapis.com/v0/b/prueba-6ec56.appspot.com/o/3.png?alt=media&token=7bf18823-1dfe-404b-9b84-e7a53127dd2f" width="1000"> 
 </p> 
 
 
 luego debemos dar click sobre el siguiente icono
 
   
 <p align="center"> 
 <img src="https://firebasestorage.googleapis.com/v0/b/prueba-6ec56.appspot.com/o/5.png?alt=media&token=7baf42e7-10dd-44c9-ac83-aecc4ede40dd" width="1000"> 
 </p> 
 
 
 nos mostrara una ventana con codigo unico de nuestro proyecto, solo deberemos copiar lo que esta marcado dentro del cuadro 
   
 <p align="center"> 
 <img src="https://firebasestorage.googleapis.com/v0/b/prueba-6ec56.appspot.com/o/6.png?alt=media&token=633ffaca-394a-44a7-9d4c-b5f5f5fe5e86" width="1000"> 
 </p> 
 
 
en el archivo main.js, el codigo que se muestra bajo el comentario de conexionn de base de datos, debemos cambiarlo por el que copiamos de la ventana anterior

   
 <p align="center"> 
 <img src="https://firebasestorage.googleapis.com/v0/b/prueba-6ec56.appspot.com/o/7.png?alt=media&token=4a442f67-7dd2-4dca-94c7-2ec81161d992" width="1000"> 
 </p> 
 
 
 Ahora nos dirigiremos al apartado de base de datos en firebase
 
 <p align="center"> 
 <img src="https://firebasestorage.googleapis.com/v0/b/prueba-6ec56.appspot.com/o/8.png?alt=media&token=a8eb0743-31be-4f64-ad07-aa2aaec5881c" width="1000"> 
 </p> 
 
 
 y buscaremos la siguiente venta y daremos click donde dice crear base de datos
 
   
 <p align="center"> 
 <img src="https://firebasestorage.googleapis.com/v0/b/prueba-6ec56.appspot.com/o/9.png?alt=media&token=f063d0e0-c2d8-4811-9c7a-1eae2e430115" width="1000"> 
 </p> 
 
 marcamos el siguiente campo y click sobre crear
 
 <p align="center"> 
 <img src="https://firebasestorage.googleapis.com/v0/b/prueba-6ec56.appspot.com/o/10.png?alt=media&token=c0673dea-da0f-401e-b14f-4be99327e44d" width="1000"> 
 </p> 
  

 Listo con esto ya tienes este repo funcionando con tu propia base de datos en firebase











